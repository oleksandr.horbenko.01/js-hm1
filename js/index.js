// Зміну в JS можна оголосити за допомогою ключового слова let та const.

// Промпт повертає текст або нуль, конфірм повертає тру або фолс.
// В конфірмі користувач може натиснути ок або скасувати, в промпті, ввести якийсь текст, нажати ок або скасувати.

// Неявне перетворення відбувається за допомогою логічних операторів( і, або, !)
// let example = 123 && 566; poverne 566
// console.log(example);

1.

let admin;
let name;
name = "Oleksandr";
admin = name;
console.log(admin);

2.

let days = 4;
days = 4 * 86400;
console.log(days);

3.

const value = prompt("Enter the value!");
console.log(value);